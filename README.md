| Caddy版本 | 编译日期        |
| ------- | ----------- |
| v2.9.1  | 2025年1月16日  |
| v2.8.1  | 2024年05月31日 |
| v2.7.6  | 2024年02月22日 |

基本上常用的系统都有了。

特性：

- brotli压缩
- cloudflare支持
- alidns支持（阿里云）
- dnspod支持（腾讯云）
- ratelimit限流
- 增加端口映射

下载的话，使用类似命令即可。个人推荐用aria2c或者wget。

Windows系统可以用powershell下载。

下载地址类似这样：

```
https://gitlab.com/0ldm0s/my_caddy/-/raw/master/<对应系统的文件>
```

例如linux amd64版的话，下载路径则为：

```shell
wget "https://gitlab.com/0ldm0s/my_caddy/-/raw/master/caddy_linux_amd64"
```

当然，也可以直接到gitee的网页上复制下载地址。

谢绝无理由转载。谢谢配合。

最后，欢迎访问我的blog：
[https://sukiyaki.su/](https://sukiyaki.su/)